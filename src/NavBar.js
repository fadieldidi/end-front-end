import React from 'react';
import './App.css';

class NavBar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            hovering : false,
            hoverCategory: null
        }
    }

    setHover(category) {
        if (category) {
            this.setState({
                hoverCategory: category,
                hovering: true
            })
        } else {
            this.setState({
                hovering: false
            })
        }
    }

    render () {
        return (
            <div className="NavBar">
                {this.props.categories.navCatagories.map((category,i) => {
                    return (<a key={i}
                        onMouseEnter={(ev) => {this.setHover(category)}}
                        onMouseLeave={(ev) => {this.setHover(null)}}
                        style={{
                            textTransform: 'uppercase',
                            fontSize: '13px',
                            letterSpacing: '.1em',
                            padding: '5px 15px 4px',
                            lineHeight: '17px'
                        }}
                    >{category.name}</a>)
                })}
                <NavCategoryExpanded category={this.state.hoverCategory} show={this.state.hovering}></NavCategoryExpanded>
            </div>
        )
    }
}

class NavCategoryExpanded extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hovering : false,
        }
    }

    setHover(state) {
        this.setState({hovering: state});
    }

    render () {
        if(this.props.show || this.state.hovering) {
            this.columns = this.props.category.children_data.reduce((prev, curr, idx) => {
                if (curr.include_in_menu_column2) {
                    if(curr.is_column_header) {
                        prev.column2Header = curr;
                    } else {
                        prev.column2.push(curr)
                    }
                } else if (curr.include_in_menu_column3) {
                    if(curr.is_column_header) {
                        prev.column2Header = curr;
                    } else {
                        prev.column3.push(curr)
                    }
                } else {
                    if(curr.is_column_header) {
                        prev.column1Header = curr;
                    } else {
                        prev.column1.push(curr)
                    }

                }
                return prev;
            }, {column1: [], column2: [], column3: [], column1Header: null, column2Header: null, colum3Header: null})
            return (
            <div style={{height: '0px'}} >
                <div style={{zIndex: 10, position: 'relative', background: 'white', display: 'flex'}}
                    onMouseEnter={(ev) => {this.setHover(true)}}
                    onMouseLeave={(ev) => {this.setHover(false)}}>
                    <div 
                        style={{
                            display: 'grid',
                            gridTemplateColumns: 'repeat(4,213px)',
                            gridColumnGap: '20px',
                            margin: '0 auto'
                            }}>
                                <ul style={{listStyle: 'none'}}>
                                    <li>{this.columns.column1Header ? this.columns.column1Header.custom_category_name : ''}</li>
                                    {this.columns.column1.map((child, i) => {
                                        return (<li><a href={child.url_path} key={i}>{child.custom_category_name ||child.name}</a></li>)
                                    })}
                                </ul>
                                <ul style={{listStyle: 'none'}}>
                                    <li>{this.columns.column2Header ? this.columns.column2Header.custom_category_name : ''}</li>
                                    {this.columns.column2.map((child, i) => {
                                        return (<li><a href={child.url_path} key={i}>{child.custom_category_name || child.name}</a></li>)
                                    })}
                                </ul>
                                <ul style={{listStyle: 'none'}}>
                                    <li>{this.columns.column3Header ? this.columns.column3Header.custom_category_name : ''}</li>
                                    {this.columns.column3.map((child, i) => {
                                        return (<li><a href={child.url_path} key={i}>{child.custom_category_name ||child.name}</a></li>)
                                    })}
                                </ul>
                    </div>
                </div>
            </div>);
        }
        else {
            return null;
        }
    }
}

const NavBarStyles = {

}

export default NavBar;
