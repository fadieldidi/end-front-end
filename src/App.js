import React from 'react';
import NavBar from './NavBar';
import './App.css';
import * as data from './data/testData';
function App() {
  console.log(data.TEST_DATA);
  return (
    <div className="App">
    <NavBar categories={data.TEST_DATA}></NavBar>
      <header className="App-header">
      </header>
    </div>
  );
}

export default App;
